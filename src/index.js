const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

const reg = require('./routes/api/register');

app.use('/api', reg);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on Port ${port}`));