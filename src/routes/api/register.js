const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

//Get User List
router.get('/users', async (req, res) => {
    const users = await loadUserCollection();
    res.send(await users.find({}).toArray());
});


//Add User
router.post('/reg', async (req, res) => {

      if (!req.body.email || !req.body.password) {
         return res.status(401).json({
            error: 'Email or Password Must not left empty!'
        })
      }

      // const user = await users.findOne({
      //    email: req.body.email
      // });
      // console.log(user)

      // if (user) {
      //    return res.status(400).json({
      //       msg: "Email already registered",
      //    });
      // } else {
         const users = await loadUserCollection();
         await users.insertOne({
            email: req.body.email,
            fullname: req.body.full_name,
            password: req.body.password,
            createdAt: new Date()
         });
         res.status(201).send();
      // }
      
});

//Delete User
router.delete('/:id', async (req, res) => {
   const users = await loadUserCollection();
   await users.deleteOne({_id: new mongodb.ObjectID(req.params.id)});
   res.status(200).send();
});
 

async function loadUserCollection() {
    const client = await mongodb.MongoClient.connect('mongodb+srv://ezwan:ezwan123@assessment.py1tl.mongodb.net/<dbname>?retryWrites=true&w=majority', {
        useNewUrlParser: true
    });

    return client.db('assessment').collection('users')
}


module.exports = router;